from user import User
from post import Post

print("-------------------------------------------------Before 2024-------------------------------------------------")
app_user_one = User("sithi@gmail.com", "Sithila SS", "sithi@2010", "student in Majno School")
app_user_one.get_user_info()
print("-------------------------------------------------After 2024-------------------------------------------------")
app_user_one.change_job_title("Software Engineer in iCET")
app_user_one.get_user_info()

print("-------------------------------------------------Before 2024-------------------------------------------------")
app_user_two = User("savi@gmail.com", "Savidya OPS", "savi@2005", "chorister in Majno School")
app_user_two.get_user_info()
print("-------------------------------------------------After 2028-------------------------------------------------")
app_user_two.change_job_title("trainer in iCET")
app_user_two.get_user_info()
print("\n")
new_post = Post("Training more than 1000 students today :)", app_user_two.name)
new_post.get_post_info()
new_post = Post("Working and earning money at home today ;)", app_user_one.name)
new_post.get_post_info()
